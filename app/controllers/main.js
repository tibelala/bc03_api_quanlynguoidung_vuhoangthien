import { NguoiDung } from "../models/nguoiDung.js";
const BASE_URL = "https://627a0d274a5ef80e2c12cbf8.mockapi.io/nguoi-dung";

let renderDanhSanhNguoiDung = async () => {
  let danhSachNguoiDungRaw = await getDanhSachNguoiDung();
  let danhSachNguoiDung = danhSachNguoiDungRaw.map((nguoiDung) => {
    return new NguoiDung(
      nguoiDung.id,
      nguoiDung.username,
      nguoiDung.name,
      nguoiDung.password,
      nguoiDung.email,
      nguoiDung.type,
      nguoiDung.language,
      nguoiDung.direction,
      nguoiDung.img
    );
  });
  let contentHTML = "";
  danhSachNguoiDung.forEach((nguoiDung) => {
    let contentTr = `
    <tr>
    <td>${nguoiDung.taiKhoan}</td>
    <td>${nguoiDung.ten}</td>
    <td>${nguoiDung.matKhau}</td>
    <td>${nguoiDung.email}</td>
    <td>${nguoiDung.hinhAnh}</td>
    <td>${nguoiDung.loai}</td>
    <td>${nguoiDung.ngonNgu}</td>
    <td>${nguoiDung.moTa}</td>
    <td>
    <button class="btn btn-danger" onclick="xoaDanhSachService(${nguoiDung.id})">Xoá</button>
    <button
    onclick="layThongTinSanPham(${nguoiDung.id})" 
    class="btn btn-primary"
    data-toggle="modal"
    data-target="#myModal">Sửa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
};
// start xoá người dùng
const xoaDanhSachService = function (id) {
  console.log("yes");
  axios({
    url: `${BASE_URL}/nguoi-dung/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderDanhSanhNguoiDung();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
// end xoá người dùng
let getDanhSachNguoiDung = async () => {
  let result = await axios.get(BASE_URL);
  console.log(result.data);
  return result.data;
};
renderDanhSanhNguoiDung();

let layThongTinTuForm = () => {
  let taiKhoan = document.getElementById("TaiKhoan").value;
  let hoTen = document.getElementById("HoTen").value;
  let matKhau = document.getElementById("MatKhau").value;
  let email = document.getElementById("Email").value;
  let hinhAnh = document.getElementById("HinhAnh").value;
  let loaiNguoiDung = document.getElementById("loaiNguoiDung").value;
  let loaiNgonNgu = document.getElementById("loaiNgonNgu").value;
  let moTa = document.getElementById("MoTa").value;
  return {
    username: taiKhoan,
    name: hoTen,
    password: matKhau,
    email: email,
    type: loaiNguoiDung,
    language: loaiNgonNgu,
    direction: moTa,
    img: hinhAnh,
  };
};
document.getElementById("themNguoiDung").addEventListener("click", function () {
  console.log("yes");
  let dataForm = layThongTinTuForm();
  console.log(dataForm);
  axios({
    url: BASE_URL,
    method: "POST",
    data: dataForm,
  })
    .then((res) => {
      $("#myModal").modal("hide");
      renderDanhSanhNguoiDung();
    })
    .catch((err) => {
      console.log(err);
    });
});
