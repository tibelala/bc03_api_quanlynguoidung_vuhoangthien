class NguoiDung {
  constructor(
    _taiKhoan,
    _ten,
    _matKhau,
    _email,
    _hinhAnh,
    _loai,
    _ngonNgu,
    _moTa
  ) {
    this.taiKhoan = _taiKhoan;
    this.ten = _ten;
    this.matKhau = _matKhau;
    this.email = _email;
    this.hinhAnh = _hinhAnh;
    this.loai = _loai;
    this.ngonNgu = _ngonNgu;
    this.moTa = _moTa;
  }
}
export { NguoiDung };
